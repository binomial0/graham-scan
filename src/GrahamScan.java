import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GrahamScan {

    public static boolean counterclockwise(Point p, Point q, Point r) {
        return (r.getY() - p.getY()) * (q.getX() - p.getX())
                > (q.getY() - p.getY()) * (r.getX() - p.getX());
    }

    public static ArrayList<Point> grahamScan(ArrayList<Point> points) {
        if (points.isEmpty()) {
            return new ArrayList<>();
        }

        // get lowest element
        int bottom_index = -1;
        double bottom_y = Double.POSITIVE_INFINITY;
        for (int i = 0; i < points.size(); i++) {
            double point_y = points.get(i).getY();
            if (point_y <= bottom_y) {
                bottom_index = i;
                bottom_y = point_y;
            }
        }
        // put leftmost element first
        Collections.swap(points, 0, bottom_index);
        Point left_point = points.get(0);

        // sort the others
        List<Point> others = points.subList(1, points.size());
        others.sort((p1, p2) -> {
            if (counterclockwise(left_point, p1, p2)) {
                return -1;
            } else {
                return 1;
            }
        });

        // actual Graham's scan
        ArrayList<Point> hull = new ArrayList<>();
        for (Point p : points) {

            removeConcaveCorners(hull, p);
            hull.add(p);
        }

        removeConcaveCorners(hull, left_point);

        return hull;
    }

    private static void removeConcaveCorners(ArrayList<Point> hull, Point next) {
        while (hull.size() >= 2) {
            Point last = hull.get(hull.size()-1);
            Point second_last = hull.get(hull.size()-2);

            if (counterclockwise(second_last, last, next)) {
                break;
            }

            hull.remove(hull.size()-1);
        }
    }

    public static void main(String[] args) {
        ArrayList<Point> points = new ArrayList<>();
        points.add(new Point(0, 0));
        points.add(new Point(0, 2));
        points.add(new Point(3, 3));
        points.add(new Point(1, 2));
        points.add(new Point(2, 1));
        points.add(new Point(1, 1));

        ArrayList<Point> hull = grahamScan(points);

        for (Point p : hull) {
            System.out.println(p);
        }
    }

    //=== Variants with maximum step count - more complicated, mainly for visualisation

    public static PartialScanResult grahamScan(ArrayList<Point> points, int max_steps) {
        if (points.isEmpty()) {
            return new PartialScanResult(new ArrayList<>(), false, false, null);
        }

        // get lowest element
        int bottom_index = -1;
        double bottom_y = Double.POSITIVE_INFINITY;
        for (int i = 0; i < points.size(); i++) {
            double point_y = points.get(i).getY();
            if (point_y <= bottom_y) {
                bottom_index = i;
                bottom_y = point_y;
            }
        }
        // put leftmost element first
        Collections.swap(points, 0, bottom_index);
        Point left_point = points.get(0);

        // sort the others
        List<Point> others = points.subList(1, points.size());
        others.sort((p1, p2) -> {
            if (counterclockwise(left_point, p1, p2)) {
                return -1;
            } else {
                return 1;
            }
        });

        if (max_steps <= points.size()) {
            if (max_steps == points.size()) {
                return new PartialScanResult(points, true, true, null);
            } else {
                return new PartialScanResult(points.subList(0, max_steps+1), false, true, null);
            }
        }
        max_steps -= points.size();

        // actual Graham's scan
        ArrayList<Point> hull = new ArrayList<>();
        Point[] coins = null;
        max_steps += 2; // place the first coins two points later
        for (Point p : points) {
            if (max_steps > 0) {
                max_steps -= 1;
                max_steps = removeConcaveCorners(hull, p, max_steps);
                if (max_steps == 0 && hull.size() >= 2) {
                    Point last = hull.get(hull.size() - 1);
                    Point second_last = hull.get(hull.size() - 2);
                    coins = new Point[]{second_last, last, p};
                }
                hull.add(p);
            } else {
                hull.add(p);
            }
        }

        if (max_steps > 0) {
            max_steps -= 1;
            max_steps = removeConcaveCorners(hull, left_point, max_steps);
            if (max_steps == 0 && hull.size() >= 2) {
                Point last = hull.get(hull.size() - 1);
                Point second_last = hull.get(hull.size() - 2);
                coins = new Point[]{second_last, last, left_point};
            }
        }

        return new PartialScanResult(hull, true, false, coins);
    }

    private static int removeConcaveCorners(ArrayList<Point> hull, Point next, int max_steps) {
        while (hull.size() >= 2 && max_steps > 0) {
            Point last = hull.get(hull.size() - 1);
            Point second_last = hull.get(hull.size() - 2);

            if (counterclockwise(second_last, last, next)) {
                break;
            }

            hull.remove(hull.size() - 1);

            max_steps -= 1;
        }
        return max_steps;
    }

}
