import java.util.List;

public class PartialScanResult {

    List<Point> hull;
    boolean close;
    boolean intermediatePoints;
    Point[] coins;

    public PartialScanResult(List<Point> hull, boolean close, boolean intermediatePoints, Point[] coins) {
        this.hull = hull;
        this.close = close;
        this.intermediatePoints = intermediatePoints;
        this.coins = coins;
    }
}
